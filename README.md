# Pipeline Trigger HOWTO


## Getting started
This uses the docs from [GitLab Docs](https://docs.gitlab.com/ee/ci/triggers/#use-a-webhook).

Try it: [https://ndren.gitlab.io/pipeline-trigger-howto/](https://ndren.gitlab.io/pipeline-trigger-howto/)
