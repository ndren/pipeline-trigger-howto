const project_id_elem = document.getElementById("project_id")
const ref_name_elem = document.getElementById("ref_name")
const token_elem = document.getElementById("token")
const output_elem = document.getElementById("output")

function updateData(_e) {
    const project_id = project_id_elem.value || "<project_id>"
    const ref_name = ref_name_elem.value || "<ref_name>"
    const token = token_elem.value || "<token>"
    output_elem.textContent = `https://gitlab.com/api/v4/projects/${project_id}/ref/${ref_name}/trigger/pipeline?token=${token}`
}
async function triggerCI() {
	await fetch(output_elem.textContent, {method: "POST"})
	alert("Finished fetch!")
}
document.querySelectorAll("input").forEach(function(elem) {elem.addEventListener("input", updateData)})
updateData(null)
